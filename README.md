# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Pavel Semikolenov
* **Company**: T1 consulting
* **E-MAIL**: pavelsemikolenov@yandex.ru
* **E-MAIL**: pavelsemikolenov@mail.ru

## SOFTWARE

* OpenJDK 8

* Intellij Idea

* MS Windows 10

## HARDWERE

* **RAM**: 14Gb
* **CPU**: i5
* **HDD**: 512Gb

## BUILD APPLICATION

```shell
mvn clean install
```

## RUN PROGRAM

```shell
java -jar ./task-manager.jar
```
