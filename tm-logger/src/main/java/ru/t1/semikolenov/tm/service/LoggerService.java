package ru.t1.semikolenov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.semikolenov.tm.api.service.ILoggerService;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
@NoArgsConstructor
public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String MONGO_HOST = "localhost";

    @NotNull
    private static final int MONGO_PORT = 27017;

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final MongoClient mongoClient = new MongoClient(MONGO_HOST, MONGO_PORT);

    @NotNull
    private final MongoDatabase mongoDatabase = mongoClient.getDatabase("tm");

    @Override
    @SneakyThrows
    public void writeLog(@NotNull final String json) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(json, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        if (mongoDatabase.getCollection(collectionName) == null) mongoDatabase.createCollection(collectionName);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
    }

}
