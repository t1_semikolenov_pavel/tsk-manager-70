package ru.t1.semikolenov.tm.api.service.model;

import ru.t1.semikolenov.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {
}
