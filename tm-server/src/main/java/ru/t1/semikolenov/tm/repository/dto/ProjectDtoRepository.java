package ru.t1.semikolenov.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1.semikolenov.tm.dto.model.ProjectDTO;

@Repository
public interface ProjectDtoRepository extends AbstractUserDtoOwnedRepository<ProjectDTO> {

}
