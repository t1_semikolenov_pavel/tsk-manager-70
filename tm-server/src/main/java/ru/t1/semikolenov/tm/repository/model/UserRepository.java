package ru.t1.semikolenov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.semikolenov.tm.model.User;

@Repository
public interface UserRepository extends AbstractRepository<User> {

    @Nullable
    User findFirstByLogin(@NotNull String login);

    @Nullable
    User findFirstByEmail(@NotNull String email);

}
