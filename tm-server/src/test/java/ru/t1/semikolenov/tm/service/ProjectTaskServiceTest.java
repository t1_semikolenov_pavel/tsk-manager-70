package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.semikolenov.tm.api.service.dto.IProjectDtoService;
import ru.t1.semikolenov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.semikolenov.tm.api.service.dto.ITaskDtoService;
import ru.t1.semikolenov.tm.api.service.dto.IUserDtoService;
import ru.t1.semikolenov.tm.configuration.ContextConfiguration;
import ru.t1.semikolenov.tm.dto.model.ProjectDTO;
import ru.t1.semikolenov.tm.dto.model.TaskDTO;
import ru.t1.semikolenov.tm.dto.model.UserDTO;
import ru.t1.semikolenov.tm.exception.entity.TaskNotFoundException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.exception.field.EmptyUserIdException;

public class ProjectTaskServiceTest {

    @NotNull
    private IProjectTaskDtoService projectTaskService;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private IProjectDtoService projectService;

    @NotNull
    private ITaskDtoService taskService;

    private String USER_ID;

    private String PROJECT_ID;

    private String TASK_ID;

    @Before
    public void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ContextConfiguration.class);
        projectTaskService = context.getBean(IProjectTaskDtoService.class);
        userService = context.getBean(IUserDtoService.class);
        projectService = context.getBean(IProjectDtoService.class);
        taskService = context.getBean(ITaskDtoService.class);
        @NotNull final UserDTO user = userService.create("user_test", "user_test");
        USER_ID = user.getId();
        @NotNull final ProjectDTO project = projectService.create(USER_ID, "project_test");
        PROJECT_ID = project.getId();
        @NotNull final TaskDTO task = taskService.create(USER_ID, "task_test");
        TASK_ID = task.getId();
    }

    @After
    public void end() {
        taskService.clear(USER_ID);
        projectService.clear(USER_ID);
        userService.removeByLogin("user_test");
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.bindTaskToProject("", PROJECT_ID, TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, "", TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, "task_id"));
        projectTaskService.bindTaskToProject(USER_ID, PROJECT_ID, TASK_ID);
        @NotNull final TaskDTO task = taskService.findOneById(TASK_ID);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(PROJECT_ID, task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        Assert.assertThrows(EmptyUserIdException.class,
                () -> projectTaskService.unbindTaskFromProject("", PROJECT_ID, TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, "", TASK_ID));
        Assert.assertThrows(EmptyIdException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, ""));
        Assert.assertThrows(TaskNotFoundException.class,
                () -> projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, "task_id"));
        projectTaskService.unbindTaskFromProject(USER_ID, PROJECT_ID, TASK_ID);
        @NotNull final TaskDTO task_unbind = taskService.findOneById(TASK_ID);
        Assert.assertNull(task_unbind.getProjectId());
    }

}
