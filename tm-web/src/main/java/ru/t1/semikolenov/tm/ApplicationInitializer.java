package ru.t1.semikolenov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.semikolenov.tm.configuration.ApplicationConfiguration;
import ru.t1.semikolenov.tm.configuration.WebApplicationConfiguration;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @NotNull
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] {ApplicationConfiguration.class};
    }

    @NotNull
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] {WebApplicationConfiguration.class};
    }

    @NotNull
    @Override
    protected String[] getServletMappings() {
        return new String[] {"/"};
    }

}

