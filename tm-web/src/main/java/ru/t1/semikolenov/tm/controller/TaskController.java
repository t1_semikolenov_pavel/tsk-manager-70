package ru.t1.semikolenov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.model.Task;
import ru.t1.semikolenov.tm.service.ProjectService;
import ru.t1.semikolenov.tm.service.TaskService;
import ru.t1.semikolenov.tm.util.UserUtil;

import java.util.List;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private TaskService taskService;

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    private List<Task> getTasks() {
        return taskService.findAllByUserId(UserUtil.getUserId());
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @ModelAttribute("projects")
    public List<Project> getProjects() {
        return projectService.findAllByUserId(UserUtil.getUserId());
    }

    @GetMapping("/task/create")
    public String create() {
        taskService.add("new task " + System.currentTimeMillis(), UserUtil.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") final String id) {
        taskService.removeByIdAndUserId(id, UserUtil.getUserId());
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute("task") final Task task, final BindingResult result) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.save(task, UserUtil.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") final String id) {
        final Task task = taskService.findByIdAndUserId(id, UserUtil.getUserId());
        return new ModelAndView("task-edit", "task", task);
    }

    @GetMapping("/tasks")
    public ModelAndView list() {
        return new ModelAndView("task-list", "tasks", getTasks());
    }

}
