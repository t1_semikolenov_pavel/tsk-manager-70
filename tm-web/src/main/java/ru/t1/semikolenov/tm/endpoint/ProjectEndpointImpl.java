package ru.t1.semikolenov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.semikolenov.tm.api.ProjectEndpoint;
import ru.t1.semikolenov.tm.model.Project;
import ru.t1.semikolenov.tm.service.ProjectService;
import ru.t1.semikolenov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/project")
@WebService(endpointInterface = "ru.t1.semikolenov.tm.api.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(projectService.findAllByUserId(UserUtil.getUserId()));
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Project findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return projectService.findByIdAndUserId(id, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        return projectService.existsByIdAndUserId(id, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectService.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public Project save(
            @NotNull
            @WebParam(name = "project")
            @RequestBody final Project project
    ) {
        return projectService.save(project, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "project")
            @RequestBody final Project project
    ) {
        projectService.remove(project, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") final String id
    ) {
        projectService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "projects")
            @RequestBody final List<Project> projects
    ) {
        projectService.remove(projects, UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        projectService.clearByUserId(UserUtil.getUserId());
    }

}
