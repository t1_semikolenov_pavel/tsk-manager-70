package ru.t1.semikolenov.tm.exception.system;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
