package ru.t1.semikolenov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.util.DateUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractModel implements Serializable {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

}
