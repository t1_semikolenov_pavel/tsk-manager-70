package ru.t1.semikolenov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.semikolenov.tm.exception.entity.TaskNotFoundException;
import ru.t1.semikolenov.tm.exception.field.EmptyIdException;
import ru.t1.semikolenov.tm.exception.field.EmptyNameException;
import ru.t1.semikolenov.tm.exception.field.EmptyUserIdException;
import ru.t1.semikolenov.tm.model.Task;
import ru.t1.semikolenov.tm.repository.TaskRepository;

import java.util.List;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    public Task add(@Nullable final String name, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task(name);
        task.setUserId(userId);
        return taskRepository.save(task);
    }

    public Task save(@Nullable final Task task, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new TaskNotFoundException();
        task.setUserId(userId);
        return taskRepository.save(task);
    }

    @NotNull
    public List<Task> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findAllByUserId(userId);
    }

    @Nullable
    public Task findByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findByIdAndUserId(id, userId).orElse(null);
    }

    public boolean existsByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.existsByIdAndUserId(id, userId);
    }

    public long countByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.countByUserId(userId);
    }

    @Transactional
    public void remove(@Nullable final Task task, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new TaskNotFoundException();
        removeByIdAndUserId(task.getId(), userId);
    }

    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteByIdAndUserId(id, userId);
    }

    @Transactional
    public void remove(@Nullable final List<Task> tasks, @Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (tasks == null) throw new TaskNotFoundException();
        tasks.forEach(task -> remove(task, userId));
    }

    public void clearByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteAllByUserId(userId);
    }

}
